<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interview;
use App\Candidate;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interviews =Interview::all(); 
        $candidates=Candidate::all(); 
        $users=Candidate::all(); 


        return view('interviews.indexint', compact('interviews','candidates','users'));
    }


    public function myInterviews()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interviews;
        //$candidates = Candidate::all();
        $users=User::all();
        /*$interviews =Interview::all(); 
        $candidates=Candidate::all(); 
        $users=Candidate::all(); */
              
        return view('interviews.indexint', compact('interviews','users'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('assign-user');
        Gate::authorize('assign-interview');
        $candidates=Candidate::all();
        $users=User::all();


        

        return view('interviews.createint', compact('candidates','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interview();
        $interview->candidate_id = $request->candidate_id; 
        $interview->user_id = $request->user_id;
        $int = $interview->create($request->all());
        $int->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeCandidate($id, $cid = null){
      
        $interview = Interview::findOrFail($id);
        $interview->candidate_id = $cid;
        $interview->save(); 
        return back();
        //return redirect('candidates');
    }


    public function changeUser($id, $uid = null){
        $interview = Interview::findOrFail($id);
        $interview->user_id = $uid;
        $interview->save(); 
        return back();
        //return redirect('candidates');
    }
}
