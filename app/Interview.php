<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $fillable = ['summary','date','candidate_id','user_id'];

    public function candidate(){
        return $this->belongsTo('App\Candidate');

    }  

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }
    
}
