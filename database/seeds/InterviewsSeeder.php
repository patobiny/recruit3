<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class InterviewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([

            ['summary' => 'The interview went good',
            'date'=>date('Y-m-d H:i:s'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()],

            ['summary' => 'She is very good for the company',
            'date'=>date('Y-m-d H:i:s'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()]


        ]);   
    }
}
